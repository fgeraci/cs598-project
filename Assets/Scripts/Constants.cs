﻿
public static class Constants {

    // Scenes
    public static string Scene_MainMenu = "Scenes/Main_Menu";

    // UI - Images
    public static string Image_StudioLogo = "UI.Image.Studio_Logo";

    // Audio
    public static string Audio_Requiem_Intro = "Sounds/Requiem_Intro";
    public static string Audio_Whispers_Intro = "Sounds/Whispers_Intro";

}
