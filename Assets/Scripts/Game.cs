﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour {

    #region Singleton
    public static Game GameController;
    #endregion

    #region Members
    public float StudioLogoFadeSpeed = 2f;
    public float AudioFadeSpeed = 2f;

    private AudioSource audioSourceIntro, audioSourceWhispers;
    private AudioClip audio_RequiemIntro, audio_WhispersIntro;
    private bool splashingLogos;
    #endregion

    #region Unity_Methods
    void Awake() {
        if (GameController == null) {
            GameController = this;
            DontDestroyOnLoad(gameObject);
            AudioSource[] sources = GetComponents<AudioSource>();
            audioSourceIntro = sources[0];
            audioSourceWhispers = sources[1];
            LoadMenuResources();
        } else if (GameController != this) Destroy(gameObject);
    }

    void Start () {
        PlayClip(audioSourceIntro,audio_RequiemIntro,true,true,true);
        StartCoroutine(RunSplash());
    }
    #endregion

    #region Game_Management
    private void PlayClip(AudioSource audioSource, AudioClip clip, bool force = false, bool loop = false, bool fadeIn = false, bool fadeOut = false) {
        if (!audioSource.isPlaying || force) {
            audioSource.loop = loop;
            audioSource.clip = clip;
            audioSource.Play();
            if (fadeIn || fadeOut)
                StartCoroutine(FadeVolume(audioSource, fadeIn));
        }
    }

    private IEnumerator FadeVolume(AudioSource audioSource, bool up) {
        float speed = AudioFadeSpeed / 100f;
        audioSource.volume = up ? 0f : 100f;
        float target = up ? 1f : 0f;
        int mod = up ? 1 : -1;
        while(audioSource.volume != target) {
            audioSource.volume = Mathf.Clamp01(audioSource.volume + (speed * mod));
            yield return null;
        }
    }

    private IEnumerator RunSplash() {
        AsyncOperation ao = SceneManager.LoadSceneAsync(Constants.Scene_MainMenu, LoadSceneMode.Single);
        ao.allowSceneActivation = false;
        WaitForSeconds wait = new WaitForSeconds(2f);
        yield return wait;
        FadeImage(Constants.Image_StudioLogo, true, StudioLogoFadeSpeed);
        wait = new WaitForSeconds(StudioLogoFadeSpeed * 2f);
        yield return wait;
        FadeImage(Constants.Image_StudioLogo, false, StudioLogoFadeSpeed * 3f);
        yield return wait;
        while (splashingLogos) {
            yield return new WaitForSeconds(1);
        }
        PlayClip(audioSourceWhispers, audio_WhispersIntro, true, true, true, false);
        ao.allowSceneActivation = true;
    }

    private void LoadMenuResources() {
        audio_RequiemIntro = Resources.Load(Constants.Audio_Requiem_Intro) as AudioClip;
        audio_WhispersIntro = Resources.Load(Constants.Audio_Whispers_Intro) as AudioClip;
    }
    #endregion

    #region Utilities
    private void FadeImage(string ObjectName, bool In, float speed) {
        speed = StudioLogoFadeSpeed / 100;
        Image image = GameObject.FindGameObjectWithTag(ObjectName).GetComponent<Image>();
        if (In)
            StartCoroutine(FadeIn(image, speed));
        else
            StartCoroutine(FadeOut(image, speed));
    }

    public IEnumerator FadeIn(UnityEngine.UI.Image image, float speed) {
        splashingLogos = true;
        while (image.color.a < 0.99f) {
            Color color = image.color;
            color.a += speed;
            image.color = color;
            yield return null;
        }
        Color c = image.color;
        c.a = 1;
        image.color = c;
    }

    public IEnumerator FadeOut(UnityEngine.UI.Image image, float speed) {
        while (image.color.a > 0.1f) {
            Color color = image.color;
            color.a -= speed;
            image.color = color;
            yield return null;
        }
        Color c = image.color;
        c.a = 0;
        image.color = c;
        splashingLogos = false;
    }
    #endregion
}
